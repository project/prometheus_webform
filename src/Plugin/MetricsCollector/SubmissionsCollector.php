<?php

declare(strict_types=1);

namespace Drupal\prometheus_webform\Plugin\MetricsCollector;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\prometheus_exporter\Plugin\BaseMetricsCollector;
use Drupal\webform\WebformEntityStorageInterface;
use PNX\Prometheus\Gauge;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Collects metrics for Webform submissions.
 *
 * @MetricsCollector(
 *   id = "webform_submissions",
 *   title = @Translation("Webform Submissions"),
 *   description = @Translation("Provides metrics for Webform submissions.")
 * )
 */
class SubmissionsCollector extends BaseMetricsCollector implements ContainerFactoryPluginInterface {

  /**
   * SubmissionsCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\webform\WebformEntityStorageInterface $webformStorage
   *   The Webform storage.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected WebformEntityStorageInterface $webformStorage,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('webform'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function collectMetrics() {
    $gauge = new Gauge($this->getNamespace(), 'total', $this->getDescription());
    $submissions = $this->webformStorage->getTotalNumberOfResults();
    foreach ($submissions as $webform_id => $total) {
      $gauge->set($total, ['webform' => $webform_id]);
    }
    $metrics[] = $gauge;
    return $metrics;
  }

}
