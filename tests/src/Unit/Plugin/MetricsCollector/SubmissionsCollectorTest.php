<?php

declare(strict_types=1);

namespace Drupal\Tests\prometheus_webform\Unit\Plugin\MetricsCollector;

use Drupal\prometheus_webform\Plugin\MetricsCollector\SubmissionsCollector;
use Drupal\Tests\UnitTestCase;
use Drupal\webform\WebformEntityStorageInterface;

/**
 * @coversDefaultClass \Drupal\prometheus_exporter\Plugin\MetricsCollector\PhpInfoCollector
 * @group prometheus_webform
 */
class SubmissionsCollectorTest extends UnitTestCase {

  /**
   * @covers ::collectMetrics
   */
  public function testCollectMetrics(): void {
    $configuration = [
      'enabled' => TRUE,
      'weight' => 0,
      'settings' => [],
    ];
    $definition = [
      'provider' => 'webform_submissions',
      'description' => 'Test description',
    ];

    $webformStorage = $this->prophesize(WebformEntityStorageInterface::class);
    $webformStorage->getTotalNumberOfResults()->willReturn([
      'foo' => 23,
      'bar' => 42,
    ]);

    $collector = new SubmissionsCollector($configuration, 'webform_submissions',
      $definition, $webformStorage->reveal());

    $metrics = $collector->collectMetrics();
    $this->assertCount(1, $metrics);

    /** @var \PNX\Prometheus\Metric $metric */
    $metric = \array_shift($metrics);
    $this->assertEquals('gauge', $metric->getType());
    $this->assertEquals('drupal_webform_submissions_total', $metric->getName());
    $this->assertEquals('Test description', $metric->getHelp());

    $labelledValues = $metric->getLabelledValues();
    $labelledValue = \array_shift($labelledValues);
    $this->assertEquals(23, $labelledValue->getValue());
    $this->assertEquals(["webform" => "foo"], $labelledValue->getLabels());

    $labelledValue = \array_shift($labelledValues);
    $this->assertEquals(42, $labelledValue->getValue());
    $this->assertEquals(["webform" => "bar"], $labelledValue->getLabels());
  }

}
